package matteopasotti.com.myevents.common;

import android.app.Application;
import android.graphics.Typeface;

import com.raizlabs.android.dbflow.config.FlowConfig;
import com.raizlabs.android.dbflow.config.FlowManager;

import matteopasotti.com.myevents.data.component.DaggerNetComponent;
import matteopasotti.com.myevents.data.component.NetComponent;
import matteopasotti.com.myevents.data.module.AppModule;
import matteopasotti.com.myevents.data.module.NetModule;

/**
 * Created by matteo.pasotti on 12/10/2017.
 */

public class MyApplication extends Application {

    private NetComponent netComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        // This instantiates DBFlow
        FlowManager.init(new FlowConfig.Builder(this).build());

        netComponent = DaggerNetComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule(Constants.BASE_URL))
                .build();
    }

    public NetComponent getNetComponent() { return netComponent; }

    public Typeface getTypefaceByName (String name) {

        Typeface font = null;
        if(name != null) {
            switch ( name ) {
                case "Raleway-Black":
                    font = Typeface.createFromAsset(getAssets(), "fonts/Raleway-Black.ttf");
                    break;
                case "Raleway-BlackItalic":
                    font = Typeface.createFromAsset(getAssets(), "fonts/Raleway-BlackItalic.ttf");
                    break;
                case "Raleway-Bold":
                    font = Typeface.createFromAsset(getAssets(), "fonts/Raleway-Bold.ttf");
                    break;
                case "Raleway-BoldItalic":
                    font = Typeface.createFromAsset(getAssets(), "fonts/Raleway-BoldItalic.ttf");
                    break;
                case "Raleway-ExtraBold":
                    font = Typeface.createFromAsset(getAssets(), "fonts/Raleway-ExtraBold.ttf");
                    break;
                case "Raleway-ExtraBoldItalic":
                    font = Typeface.createFromAsset(getAssets(), "fonts/Raleway-ExtraBoldItalic.ttf");
                    break;
                case "Raleway-ExtraLight":
                    font = Typeface.createFromAsset(getAssets(), "fonts/Raleway-ExtraLight.ttf");
                    break;
                case "Raleway-ExtraLightItalic":
                    font = Typeface.createFromAsset(getAssets(), "fonts/Raleway-ExtraLightItalic.ttf");
                    break;
                case "Raleway-Italic":
                    font = Typeface.createFromAsset(getAssets(), "fonts/Raleway-Italic.ttf");
                    break;
                case "Raleway-Light":
                    font = Typeface.createFromAsset(getAssets(), "fonts/Raleway-Light.ttf");
                    break;
                case "Raleway-LightItalic":
                    font = Typeface.createFromAsset(getAssets(), "fonts/Raleway-LightItalic.ttf");
                    break;
                case "Raleway-Medium":
                    font = Typeface.createFromAsset(getAssets(), "fonts/Raleway-Medium.ttf");
                    break;
                case "Raleway-MediumItalic":
                    font = Typeface.createFromAsset(getAssets(), "fonts/Raleway-MediumItalic.ttf");
                    break;
                case "Raleway-Regular":
                    font = Typeface.createFromAsset(getAssets(), "fonts/Raleway-Regular.ttf");
                    break;
            }
        }

        return font;
    }
}
