package matteopasotti.com.myevents.data.component;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Component;
import matteopasotti.com.myevents.data.module.AppModule;
import matteopasotti.com.myevents.data.module.NetModule;
import retrofit2.Retrofit;


@Singleton
@Component(modules = {AppModule.class, NetModule.class})
public interface NetComponent {

    Retrofit retrofit();
    Application provideApplication();
}
