package matteopasotti.com.myevents.pojo;


import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EventResponse {

    @SerializedName("pagination")
    private Pagination pagination;

    @SerializedName("events")
    private List<Event> events;

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }


}
