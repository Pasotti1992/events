package matteopasotti.com.myevents.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by matteo.pasotti on 12/10/2017.
 */

public class Time implements Serializable {

    @SerializedName("timezone")
    private String timezone;

    @SerializedName("local")
    private String local;

    @SerializedName("utc")
    private String utc;

    public Time(){}

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getUtc() {
        return utc;
    }

    public void setUtc(String utc) {
        this.utc = utc;
    }


}
