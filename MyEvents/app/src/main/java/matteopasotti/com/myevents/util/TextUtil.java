package matteopasotti.com.myevents.util;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TextUtil {

    public static String fromDateToString( Date date ) {

        if( date != null ) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            return dateFormat.format(date);
        } else {
            return null;
        }

    }

    public static Date fromStringToDate (String text) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        try {
            Date date = dateFormat.parse(text);
            return date;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String formatDate(String text) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        try {
            Date date = dateFormat.parse(text);
            if(date != null ) {
                return dateFormat.format(date);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }
}
