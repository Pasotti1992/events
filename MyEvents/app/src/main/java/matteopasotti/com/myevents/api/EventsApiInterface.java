package matteopasotti.com.myevents.api;


import matteopasotti.com.myevents.pojo.EventResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface EventsApiInterface {

    @GET("events/search")
    Call<EventResponse> getEvents(@Query("token") String token, @Query("page") int page);
}
