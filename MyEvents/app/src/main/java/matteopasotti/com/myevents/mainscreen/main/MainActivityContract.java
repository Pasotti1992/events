package matteopasotti.com.myevents.mainscreen.main;

import java.util.List;

import matteopasotti.com.myevents.pojo.Event;

/**
 * Created by matteo.pasotti on 12/10/2017.
 */

public interface MainActivityContract {

    interface View {

        void initEventsList();

        void setEvents(List<Event> events);

        void updateEvents(List<Event> events);

        void showError(String message);

        void showToast(String message);

        void showLoading(boolean loading);
    }

    interface Presenter {

        void getEvents(int page);
    }
}
