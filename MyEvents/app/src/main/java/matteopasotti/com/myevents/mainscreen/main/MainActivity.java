package matteopasotti.com.myevents.mainscreen.main;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import matteopasotti.com.myevents.R;
import matteopasotti.com.myevents.common.MyApplication;
import matteopasotti.com.myevents.data.component.DaggerMainActComponent;
import matteopasotti.com.myevents.data.module.MainModule;
import matteopasotti.com.myevents.pojo.Event;

public class MainActivity extends AppCompatActivity implements MainActivityContract.View {

    @Inject
    MainPresenter presenter;

    @Inject
    EventAdapter eventAdapter;

    @Inject
    LinearLayoutManager layoutManager;

    @BindView(R.id.eventsRv)
    RecyclerView eventsRv;

    @BindView(R.id.barLoading)
    ProgressBar progressBar;

    private int page = 1;

    private int  pastVisiblesItems, visibleItemCount, totalItemCount;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        DaggerMainActComponent.builder()
                .netComponent(((MyApplication) getApplicationContext()).getNetComponent())
                .mainModule(new MainModule(this, this))
                .build().inject(this);

        initEventsList();
        presenter.getEvents(page);
    }


    @Override
    public void initEventsList() {
        eventsRv.setAdapter(eventAdapter);
        eventsRv.setLayoutManager(layoutManager);
        eventsRv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if( dy > 0 ) {
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

                    if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                        page++;
                        presenter.getEvents(page);
                    }
                }
            }
        });
    }

    @Override
    public void setEvents(List<Event> events) {
        eventAdapter.initEventList(events);
    }

    @Override
    public void updateEvents(List<Event> events) {
        eventAdapter.updateEventList(events);
    }

    @Override
    public void showError(String message) {
        Toast.makeText(getApplicationContext(), "Error " + message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoading(boolean loading) {
        eventsRv.setVisibility(loading ? View.GONE : View.VISIBLE);
        progressBar.setVisibility(loading ? View.VISIBLE : View.GONE);
    }
}
