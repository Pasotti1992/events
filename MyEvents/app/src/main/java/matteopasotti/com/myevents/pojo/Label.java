package matteopasotti.com.myevents.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by matteo.pasotti on 12/10/2017.
 */

public class Label implements Serializable {

    @SerializedName("text")
    private String text;

    @SerializedName("html")
    private String html;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }


}
