package matteopasotti.com.myevents.data.component;

import android.content.Context;

import dagger.Component;
import matteopasotti.com.myevents.data.module.MainModule;
import matteopasotti.com.myevents.mainscreen.main.MainActivity;
import matteopasotti.com.myevents.util.MainScope;


@Component(dependencies = NetComponent.class, modules = MainModule.class)
@MainScope
public interface MainActComponent {

    void inject(MainActivity mainActivity);

}
