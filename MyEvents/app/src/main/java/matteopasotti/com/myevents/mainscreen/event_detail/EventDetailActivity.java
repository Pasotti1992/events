package matteopasotti.com.myevents.mainscreen.event_detail;


import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import matteopasotti.com.myevents.R;
import matteopasotti.com.myevents.pojo.Event;

public class EventDetailActivity extends AppCompatActivity {

    public static final String EVENT_DETAIL = "EVENT_DETAIL";

    private Event event;

    @BindView(R.id.imageEventDetail)
    ImageView imageView;

    @BindView(R.id.wvDetail)
    WebView webView;

    @BindView(R.id.toolbarDetailEvent)
    Toolbar toolbar;

    @BindView(R.id.buttonEvent)
    Button buttonEvent;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_event);
        ButterKnife.bind(this);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Intent intent = getIntent();
        if( intent.getExtras() != null ) {
            event = (Event) intent.getSerializableExtra(EVENT_DETAIL);
        }

        initView();
    }


    private void initView() {

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //set image
        if( event.getLogo() != null && event.getLogo().getUrl() != null) {
            Uri imageUri = Uri.parse(event.getLogo().getUrl());
            Glide.with(this).load(imageUri)
                    .thumbnail(0.5f)
                    .crossFade()
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imageView);
        }

        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.loadData(event.getDescription().getHtml(), "text/html", "UTF-8");
    }

    @OnClick(R.id.buttonEvent)
    public void openEventBrite(){

        Intent browse = new Intent( Intent.ACTION_VIEW , Uri.parse( event.getUrl() ) );
        startActivity( browse );
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
