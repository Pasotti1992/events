package matteopasotti.com.myevents.pojo;


import com.google.gson.annotations.SerializedName;

public class Pagination {

    @SerializedName("object_count")
    private int object_count;

    @SerializedName("page_number")
    private int page_number;

    @SerializedName("page_size")
    private int page_size;

    @SerializedName("page_count")
    private int page_count;

    @SerializedName("has_more_items")
    private boolean has_more_items;

    public int getObject_count() {
        return object_count;
    }

    public void setObject_count(int object_count) {
        this.object_count = object_count;
    }

    public int getPage_number() {
        return page_number;
    }

    public void setPage_number(int page_number) {
        this.page_number = page_number;
    }

    public int getPage_size() {
        return page_size;
    }

    public void setPage_size(int page_size) {
        this.page_size = page_size;
    }

    public int getPage_count() {
        return page_count;
    }

    public void setPage_count(int page_count) {
        this.page_count = page_count;
    }

    public boolean isHas_more_items() {
        return has_more_items;
    }

    public void setHas_more_items(boolean has_more_items) {
        this.has_more_items = has_more_items;
    }



}
