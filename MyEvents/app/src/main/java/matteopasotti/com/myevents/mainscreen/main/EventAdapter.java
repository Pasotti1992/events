package matteopasotti.com.myevents.mainscreen.main;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import matteopasotti.com.myevents.R;
import matteopasotti.com.myevents.mainscreen.event_detail.EventDetailActivity;
import matteopasotti.com.myevents.pojo.Event;
import matteopasotti.com.myevents.util.TextUtil;

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.EventHolder>{

    private List<Event> events = new ArrayList<>();

    private MainActivity activity;

    @Inject
    public EventAdapter(MainActivity mainActivity) {
        this.activity = mainActivity;
    }

    @Override
    public EventHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.event_row, parent, false);
        final EventHolder holder = new EventHolder(view);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Event eventClicked = events.get(holder.getAdapterPosition());
                Intent intent = new Intent(activity, EventDetailActivity.class);
                intent.putExtra(EventDetailActivity.EVENT_DETAIL, eventClicked);

                //Animation
                ActivityOptionsCompat options = ActivityOptionsCompat.
                        makeSceneTransitionAnimation(activity, holder.imageEvent, "imageE");

                activity.startActivity(intent, options.toBundle());
            }
        });

        return holder;
    }

    @Override
    public void onBindViewHolder(EventHolder holder, int position) {


        Event event = events.get(position);

        if(event.getLogo() != null && event.getLogo().getUrl() != null) {
            Uri imageUri = Uri.parse(event.getLogo().getUrl());
            Glide.with(activity).load(imageUri)
                    .thumbnail(0.5f)
                    .crossFade()
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.imageEvent);
        } else {
            Uri imageUri = Uri.parse("http://www.pixedelic.com/themes/geode/demo/wp-content/uploads/sites/4/2014/04/placeholder2.png");
            Glide.with(activity).load(imageUri)
                    .thumbnail(0.5f)
                    .crossFade()
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.imageEvent);
        }


        holder.titleEvent.setText(event.getName().getText());
        holder.whereEvent.setText(event.getStart().getTimezone());

        if(event.getStart() != null && event.getStart().getLocal() != null && event.getEnd() != null && event.getEnd().getLocal() != null) {

            holder.startDate.setText(TextUtil.formatDate(event.getStart().getLocal()).toString()+ " - ");
            holder.endDateEvent.setText(TextUtil.formatDate(event.getEnd().getLocal()).toString());
        } else {
            holder.startDate.setText("none");
            holder.endDateEvent.setText("none");
        }

    }

    public void initEventList(List<Event> events) {
        this.events.clear();
        this.events.addAll(events);
        notifyDataSetChanged();
    }

    public void updateEventList(List<Event> events) {
        this.events.addAll(events);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return events.size();
    }

    public static class EventHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imageEvent)
        ImageView imageEvent;

        @BindView(R.id.titleEvent)
        TextView titleEvent;

        @BindView(R.id.whereEvent)
        TextView whereEvent;

        @BindView(R.id.startDateEvent)
        TextView startDate;

        @BindView(R.id.endDateEvent)
        TextView endDateEvent;

        public EventHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
