package matteopasotti.com.myevents.data.module;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;

import dagger.Module;
import dagger.Provides;
import matteopasotti.com.myevents.mainscreen.main.EventAdapter;
import matteopasotti.com.myevents.mainscreen.main.MainActivity;
import matteopasotti.com.myevents.mainscreen.main.MainActivityContract;
import matteopasotti.com.myevents.util.ActivityContext;
import matteopasotti.com.myevents.util.MainScope;

@Module
public class MainModule {

    private final MainActivityContract.View view;

    private MainActivity activity;

    public MainModule(MainActivityContract.View view, MainActivity mainActivity) {

        this.view = view;
        this.activity = mainActivity;
    }

    @Provides
    @MainScope
    MainActivityContract.View provideMainView(){return view;}

    @Provides
    @MainScope
    EventAdapter provideEventsAdapter(MainActivity mainActivity){
        return new EventAdapter(mainActivity);
    }

    @Provides
    @MainScope
    MainActivity provideMainActivity() {
        return activity;
    }

    @Provides
    @MainScope
    LinearLayoutManager provideLinearLayoutManager(MainActivity mainActivity) {
        return new LinearLayoutManager(mainActivity);
    }

}
