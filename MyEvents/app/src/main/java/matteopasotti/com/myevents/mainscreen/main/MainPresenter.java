package matteopasotti.com.myevents.mainscreen.main;


import android.app.Application;
import android.util.Log;

import java.util.List;

import javax.inject.Inject;

import matteopasotti.com.myevents.api.EventsApiInterface;
import matteopasotti.com.myevents.common.Constants;
import matteopasotti.com.myevents.pojo.Event;
import matteopasotti.com.myevents.pojo.EventResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainPresenter implements MainActivityContract.Presenter {

    Retrofit retrofit;

    Application application;

    MainActivityContract.View mView;

    @Inject
    public MainPresenter(Retrofit retrofit, Application application, MainActivityContract.View view) {
        this.retrofit = retrofit;
        this.application = application;
        this.mView = view;
    }

    @Override
    public void getEvents(int page) {

        mView.showLoading(true);
        retrofit.create(EventsApiInterface.class).getEvents(Constants.AUTH_TOKEN, page).enqueue(new Callback<EventResponse>() {
            @Override
            public void onResponse(Call<EventResponse> call, Response<EventResponse> response) {
                List<Event> events = response.body() != null ? response.body().getEvents() : null;

                mView.showLoading(false);
                if (events != null) {
                    Log.d("MainPresenter", "getEvents SUCCESS");
                    if (response.body().getPagination().getPage_count() == 1) {
                        mView.setEvents(events);
                    } else {
                        mView.updateEvents(events);
                    }

                } else {
                    Log.d("MainPresenter", "getEvents FAILED");
                    mView.showError("getEvents FAILED");
                }
            }

            @Override
            public void onFailure(Call<EventResponse> call, Throwable t) {
                mView.showLoading(false);
                Log.d("MainPresenter", "getEvents FAILED");
                mView.showError("getEvents FAILED");
            }
        });
    }
}
